<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Testimoni;

class TestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimoni = Testimoni::all();
        return view('needs/index', ['testimoni' => $testimoni]);
    }
    public function create()
    {
        return view('needs.testi');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        Testimoni::create([

            'nama' => $request->nama,
            'testimoni' => $request->testimoni

        ]);

        return redirect('/needs')->with('status', 'Testimoni kamu berhasil direkam!');
    }}